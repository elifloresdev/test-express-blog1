const express = require('express');
const path = require('path');
const favicon = require('serve-favicon');
const logger = require('morgan');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');



//mongoose
const mongoose = require('mongoose')
// 'mongodb://localhost:27017/dbTwo'

// single server
const uri = 'mongodb://localhost:27017/dbTwo';
mongoose.connect(uri,{ useMongoClient: true});

const db = mongoose.connection

db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
  console.log('db connected')
})

//<!-- mongoose end -->

const index = require('./routes/index');
const profile = require('./routes/profile');
const create = require('./routes/create');

const app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');

// session
const session = require('express-session');
const MongoStore = require('connect-mongo')(session);

app.use(session({
  secret: 'something-awesome',
  resave: true,
  saveUninitialized: false,
  store: new MongoStore({mongooseConnection: db})
}))


// session end


// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: false
}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));


//session middleware
app.use((req,res,next) => {
  res.locals.currentUser = req.session.userId
  console.log(res.locals.currentUser)
  next()
})


app.use('/', index);
app.use('/profile', profile);
app.use('/create', create);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  const err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error', {title: 'ERROR'});
});

app.listen(3000, () => {
  console.log("Express app listening at port: 3000")
})

module.exports = app;