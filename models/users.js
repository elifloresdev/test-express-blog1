'use strict'
const mongoose = require('mongoose')
const Schema = mongoose.Schema
const bcrypt = require('bcrypt')

const UserSchema = new Schema({ 

  name:  {
    type:String,
    required:true,
    trim: true
  },
  email: {
    type:String,
    required:true,
    trim: true,
    unique: true
  },
  password: { 
    type: String, 
    required:true
   }

})

UserSchema.statics.authenticate = function(email,password,callback) {
  this.findOne({email:email}).exec( (error, user) => {
      if (error) {
        return callback(error)
      } else if(!user) {
        const error = new Error('User not Found')
        error.status = 401
       return callback(error)
      } 

          bcrypt.compare(password, user.password, function(err, res) {
            if(res === true) {
              return callback(null,user)
            } else {
              return callback(error)
            }
          });
  
  })
} 


UserSchema.pre('save', function(next) {
  const user = this
  bcrypt.hash(user.password,10, function(err,hash){
    if(err) {
      return next(err)
    } else {
      user.password = hash
      next()
    }
  })
})

const User = mongoose.model('User',UserSchema)

module.exports = User