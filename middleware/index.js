const express = require('express')
const router = express.Router()

exports.inputRequirements = function(req,res,next) {

  if(req.body.title.length > 5 &&  req.body.blog.length > 10) {
    return next()
  } else {

    const error =  new Error('length too short')
    error.status = 404
    next(error)
  }

}


exports.userRequirements = function(req,res,next) {
 
  if(req.body.name !=='' && req.body.email !=='' && 
     req.body.password !== '' && req.body.confirmPassword !== '') {
        
      if(req.body.password === req.body.confirmPassword) {
          next()
      } else {
          const error = new Error('Password Doesn\'t Match')
          error.status = 404
          next(error)
      }

    } else {
        const error = new Error('No Empty Fields')
        error.status = 404
        next(error)
    }
  
}

exports.provideLogin = function(req,res,next) {

  if(req.body.email && req.body.password) {
    next()
  } else {
    const error = new Error('Provide email and password')
    error.status = 404
    next(error)
  }


}