const express = require('express');
const router = express.Router();
const mid = require('../middleware')
const User = require('../models/users')

/* GET users listing. */
router.get('/', function(req, res, next) {


  if(req.session && req.session.userId) {
    User.findById(req.session.userId).exec(function(err, user) {
      if(err) {
        return next(err)
      } else {
        res.locals.user = user
        return res.render('profile',{title: 'Profile', user:user,profile:true})
      }

    })
  } else {
      return res.render('profile',{title: 'Profile',profile:true})
  }

});

router.post("/", mid.userRequirements, (req,res,next) => {

  const userData = {
    name: req.body.name,
    email: req.body.email,
    password: req.body.password

  }

  User.create(userData,(error,user) => {
    if(error){
      next(error)
    } else {
      req.session.userId = user._id
      return res.redirect('profile')
    }
  })

  
})

module.exports = router;