const express = require('express')
const router = express.Router()
const Blog = require('../models/blogs')
const User = require('../models/users')
const mid = require('../middleware')

/* GET home page. */
router.get('/', function(req, res, next) {

  Blog.find({},(err,blogs) => {

      if (blogs.length <= 0) {

        return res.render('index', {
            message:'No blogs created at the moment',
            index:true
          });
      } else {
        return res.render('index',{blogs: blogs, index:true});
    }
    })


});


router.post('/login', mid.provideLogin, (req,res,next) => {

    const email = req.body.email
    const password = req.body.password

    User.authenticate(email,password,function(err,user) {
      if(err || !user) {
        const error = new Error('User not found or wrong password')
        error.status = 401
        next(error)
      } else {
        req.session.userId = user._id
        return res.redirect('/profile')
      }
      
    })
})


router.post('/logout', (req,res,next) => {
  if(req.session.userId) {
    req.session.destroy(error => {
      if (error) {
        return next(error)
      } else {
        res.redirect('/')
      }
    })
  }
})
module.exports = router;
