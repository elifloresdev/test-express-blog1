const express = require('express');
const router = express.Router();
const mid = require('../middleware')
const Blog = require('../models/blogs')


/* GET /Create */
router.get('/', function(req, res, next) {

  console.log('create Status Code',res.statusCode)


  return res.render('create', { title: 'Create Blog', create:true });
});


router.post('/',mid.inputRequirements, function(req, res, next) {
  console.log('/create //POST')
   
  const blogData = {
      title: req.body.title,
      blog: req.body.blog
  }

  Blog.create(blogData, (err, blog) => {
    if(err) {
      next(err)
    } else {

      return res.redirect('/')
  
    }

  })
});

module.exports = router;